﻿// CDialogMain.cpp: 实现文件
//

#include "pch.h"
#include "CDialogMain.h"
#include "afxdialogex.h"
#include "resource.h"


// CDialogMain 对话框

IMPLEMENT_DYNAMIC(CDialogMain, CDialogEx)

CDialogMain::CDialogMain(CWnd* pParent /*=nullptr*/)
	: CDialogEx(DEBUG_MAIN, pParent)
{

}

CDialogMain::~CDialogMain()
{
}

BOOL CDialogMain::OnInitDialog()
{
//     setGlobalHwnd(hDlg);
    HWND hDlg = this->GetSafeHwnd();
    SetDlgItemText(SQL_TEXT, L"select * from sqlite_master");
    SetDlgItemText(SQL_RESULT, L"hook写入成功，已开始监听数据库句柄");
    SetDlgItemText(RECEIVE_WXID_TEXT, L"filehelper");
    return TRUE;
    //初始化消息接收list
    LV_COLUMN msgPcol = { 0 };
    LPCWSTR msgTitle[] = { L"类型",L"self",L"来源",L"发送者", L"详情" };
    int msgCx[] = { 40,40,80,80,200 };
    msgPcol.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT;
    msgPcol.fmt = LVCFMT_LEFT;
    for (unsigned int i = 0; i < _countof(msgTitle); i++) {
        msgPcol.pszText = (LPWSTR)msgTitle[i];
        msgPcol.cx = msgCx[i];
        // ListView_InsertColumn(GetDlgItem(hDlg, RECIEVE_MSG_LIST), i, &msgPcol);
        listCtrlRecieveMsg_.InsertColumn(i, &msgPcol);
    }
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(RECIEVE_MSG_LIST);

    //初始化好友列表list
    LV_COLUMN friendPcol = { 0 };
    LPCWSTR friendTitle[] = { L"wxid",L"账号",L"昵称",L"备注",L"头像" };
    int friendCx[] = { 80,80,80,80,80 };
    friendPcol.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT;
    friendPcol.fmt = LVCFMT_LEFT;
    for (unsigned int i = 0; i < _countof(friendTitle); i++) {
        friendPcol.pszText = (LPWSTR)friendTitle[i];
        friendPcol.cx = friendCx[i];
        listCtrlFriend_.InsertColumn(i, &msgPcol);
        // ListView_InsertColumn(GetDlgItem(hDlg, FRIEND_LIST), i, &friendPcol);
    }

//     HANDLE lThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)getLoginStatus, NULL, NULL, 0);
//     if (lThread != 0) {
//         CloseHandle(lThread);
//     }
// 
//     HANDLE hookThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)inLineHook, NULL, NULL, 0);
//     if (hookThread != 0) {
//         CloseHandle(hookThread);
//     }
    return (INT_PTR)TRUE;

    return TRUE;
}

void CDialogMain::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, FRIEND_LIST, listCtrlFriend_);
    DDX_Control(pDX, RECIEVE_MSG_LIST, listCtrlRecieveMsg_);
}


BEGIN_MESSAGE_MAP(CDialogMain, CDialogEx)
END_MESSAGE_MAP()


// CDialogMain 消息处理程序
