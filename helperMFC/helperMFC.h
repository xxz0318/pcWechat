﻿// helperMFC.h: helperMFC DLL 的主标头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"		// 主符号


// ChelperMFCApp
// 有关此类实现的信息，请参阅 helperMFC.cpp
//

class ChelperMFCApp : public CWinApp
{
public:
	ChelperMFCApp();

// 重写
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
